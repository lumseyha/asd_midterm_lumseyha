/**
 * @format
 */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import ScreenLogin from './Components/ScreenLogin'
AppRegistry.registerComponent(appName, () => ScreenLogin);
