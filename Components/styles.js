import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    borderColor: '#ffffff',
    backgroundColor:'#ffffff',
    borderWidth: 1,
    margin: 10,
    width: 300,
    fontSize: 18,
    borderRadius: 25,
    color:'#000000'
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  signupTxt: {
    marginTop: 45,
  },
  icon: {
    width: 150,
    height: 150,
    marginBottom: 10,
  },
});

export default styles;
