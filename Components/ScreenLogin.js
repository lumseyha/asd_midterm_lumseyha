import React, {useState} from 'react';
import {Text, View, Image, ImageBackground} from 'react-native';
import MyTextInput from './TextInput';
import Button from './Button';
import styles from './styles';
export default props => {
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();

  const login = () => {
    props.navigation.navigate('ScreenProfile', {
      user: username,
      pass: password,
    });
  };
  return (
    <ImageBackground source={image} style={styles.image}>
      <View style={styles.container}>
        <MyTextInput
          style={styles.input}
          placeholder="User Name"
          onChangeText={setUsername}
          validate={true}
        />
        <MyTextInput
          style={styles.input}
          placeholder="Password"
          type="password"
          onChangeText={setPassword}
          secureTextEntry={true}
          validate={true}
        />
        <Button textStyle={{color: 'white'}} btnLogin={login}>
          Login
        </Button>
        <Text style={styles.signupTxt}>If you don't have an account yet?</Text>
        <Text style={{color: 'blue'}}>Signup</Text>
      </View>
    </ImageBackground>
  );
};
const image = {
  uri: 'https://www.wallpapertip.com/wmimgs/0-3941_whatsapp-wallpaper-painting.jpg',
};
