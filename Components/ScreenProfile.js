import React from 'react';
import {
  Text,
  View,
  Image,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {SCREEN_NAME} from './Index';

export default props => {
  const Logout = () => {
    props.navigation.reset({
      index: 0,
      routes: [{name: SCREEN_NAME.LOGIN}],
    });
  };
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.userInfoSection}>
        <View style={{flexDirection: 'row', marginTop: 15}}>
          <Image
            style={styles.iconprofile}
            source={{
              uri:
                'https://www.pngfind.com/pngs/m/80-804949_profile-icon-for-the-politics-category-circle-hd.png',
            }}
          />
          <View style={{marginLeft: 18}}>
            <Text style={styles.title}>{props.route.params.user}</Text>
            <Text style={styles.caption}>@_name</Text>
          </View>
        </View>
      </View>

      <View style={styles.userInfoSection}>
        <View style={styles.row}>
          <Icon name="map-marker-radius" color="gray" size={18} />
          <Text style={styles.titleaddress}>Preah Vihear, Cambodia</Text>
        </View>
        <View style={styles.row}>
          <Icon name="phone" color="gray" size={18} />
          <Text style={styles.titleaddress}>098631540</Text>
        </View>
        <View style={styles.row}>
          <Icon name="email" color="gray" size={18} />
          <Text style={styles.titleaddress}>lumseyha@gmail.com</Text>
        </View>
      </View>

      <View style={styles.infoBoxWrapper}>
        <View
          style={StyleSheet.compose(
            styles.infoBox,
            styles.borderbox,
          )}>
          <Text>$99</Text>
          <Text>WALLET</Text>
        </View>
        <View style={styles.infoBox}>
          <Text>2</Text>
          <Text>ORDER</Text>
        </View>
      </View>
      <View style={styles.menuWrapper}>
        <TouchableOpacity style={styles.menuItem}>
          <Icon name="heart-outline" color="#a52a2a" size={18} />
          <Text style={styles.menuItemText}>My Favorites</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.menuWrapper}>
        <TouchableOpacity style={styles.menuItem}>
          <Icon name="truck-delivery" color="#a52a2a" size={18} />
          <Text style={styles.menuItemText}>Delivery History</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.menuWrapper}>
        <TouchableOpacity style={styles.menuItem}>
          <Icon name="help-rhombus" color="#a52a2a" size={18} />
          <Text style={styles.menuItemText}>Settings</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.menuWrapper}>
        <TouchableOpacity style={styles.menuItem}>
          <Icon name="logout" color="#0000ff" size={18} />
          <Text style={styles.menuItemText} onPress={Logout}>
            Logout
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  iconprofile: {
    width: 80,
    height: 80,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 15,
    marginBottom: 5,
  },
  userInfoSection: {
    paddingHorizontal: 30,
    marginBottom: 25,
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    fontWeight: '500',
  },
  row: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  infoBoxWrapper: {
    borderBottomColor: '#ddd',
    borderBottomWidth: 1,
    borderTopColor: '#ddd',
    borderTopWidth: 1,
    flexDirection: 'row',
    height: 100,
  },
  infoBox: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuWrapper: {
    marginTop: 10,
  },
  menuItem: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  menuItemText: {
    marginTop: -3,
    marginLeft: 20,
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 26,
  },
  titleaddress: {
    color: 'gray',
    marginLeft: 20,
  },
  borderbox: {
    borderRightColor: '#ddd',
    borderRightWidth: 1,
  },
});
